from typing import Type

from pydantic import BaseModel


class RouteConfig(BaseModel):
    create_model: Type
    update_model: Type
    response_model: Type
    repository: Type

    create: bool = True
    update: bool = True
    retrieve: bool = True
    get_all: bool = True
    delete: bool = True
