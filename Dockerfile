FROM python:3.11

RUN mkdir /app

COPY . .

COPY pyproject.toml /app

RUN pip install poetry
RUN poetry install

